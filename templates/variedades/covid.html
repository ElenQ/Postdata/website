{% extends("./_base.html") %}
{% import "../_globals.html" as glob %}

{% set post_header=_("Calcular una pandemia") %}
{% set title=_("Proyectos - Variedades ") + " - " + post_header %}

{# Don't translate the date and use ISO format, the feed needs the ISO to build
the RFC822 datestring #}
{% set post_date="2020-04-10"%}

{% set post_link="variedades/covid.html" %}

{% set post_shortdesc %}{# Plain text description, it's used in Open Graph #}
Pequeña disertación sobre la cobertura mediática y la representación gráfica
de los datos de la pandemia global de la enfermedad COVID-19 que está teniendo
lugar.
{% endset %}
{% set post_shortdesc=_(post_shortdesc) %}

{% set post_content %}

<p class="text-gray subtitle">{% filter translate %}
Estamos hartos de las gráficas, de las
explicaciones y de los estudios sobre la propagación del virus SARS-CoV-2 y de
la enfermedad que provoca, por la que es más conocido: COVID-19.
{% endfilter %}</p>

{% filter markdown %}
{% filter translate %}
Estamos hartos de las noticias alarmistas, de las que dicen que esto no es
nada y de las explicaciones vacías.

Pero estamos cansados de estar en casa encerrados en cuarentena y, como
siempre, algo teníamos que hacer. Porque parece que no vives si no haces cosas.
Y hemos decidido hacer algo.

Hemos decidido meternos donde no nos llaman, en camisas de once varas, porque
nos gustan las matemáticas lo suficiente como para que no nos satisfaga lo que
vemos por ahí. Y, a pesar de no ser epidemiólogos, ni nada que se le parezca,
creemos que podemos comprender este fenómeno simplemente centrándonos en un par
de conceptos principales. Esta vez, el uso de la primera persona del plural te
incluye a ti, que, por la razón que sea, nos lees.

Aunque hay muchos modos de calcular este tipo de cosas, siendo quizás el más
comprensible por quien no se dedica al tema (como nosotros mismos) [el modelo
SIR](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology),
podemos llegar a simplificar mucho las cuentas para no perdernos en las
abrumadoras cifras que tenemos disponibles.

Antes de entrar en ello cabe aclarar algo. Está claro que la vida humana tiene
un valor incalculable y que la muerte de una única persona ya es dramática,
pero estamos hablando de números y de evoluciones. En ocasiones hay que dejar
esos temas de lado para tener una buena visión de conjunto y si en algún
momento se llega a decir que cierto número de muertes o infectados es una buena
señal, se debe recordar el contexto en el que estamos. Bajo ninguna
circunstancia será una buena señal la infección o la muerte de un ser humano,
pero nos referimos, en todo caso, a la tendencia general de la epidemia.
Empeorar menos que ayer es una buena noticia, aunque es mejor noticia no
empeorar y mucho mejor noticia será mejorar.

Dicho esto, ataquemos las cifras con las que día tras día nos abruman los
medios de comunicación.

## El crecimiento exponencial

En primer lugar tratemos de modelizar, de la forma más sencilla posible, la
expansión de la enfermedad. Suponiendo que una persona infectada, de media,
infecta a *x* personas al día, es posible deducir, de forma general, cuánta
gente acabará siendo infectada mañana:

<p class="formula"><span class="math">Infectados<sub>mañana</sub> = Infectados<sub>hoy</sub> · <em>x</em></span></p>

Para este primer acercamiento usamos los días, porque los datos de los medios
suelen ser diarios, pero podría generalizarse para cualquier frecuencia de
muestreo. Nos quedaremos los días de momento.

Siguiendo esa fórmula podemos partir del primer caso de la epidemia, y
presentarla del siguiente modo, sabiendo que el día 0 sólo había un
infectado:

<p class="formula">
<span class="math">Infectados<sub>día1</sub> = 1 · x = x</span>
</p>

Y seguir:

<p class="formula">
<span class="math">Infectados<sub>día2</sub> = Infectados<sub>día1</sub> · x = (1 · x) · x = x<sup>2</sup></span>
</p>

<p class="formula">
<span class="math">Infectados<sub>día3</sub> = Infectados<sub>día2</sub> · x = (Infectados<sub>día1</sub> · x) · x = ((1 · x) · x) · x = x<sup>3</sup></span>
</p>

Cada día que pase se multiplicará por *x* de nuevo. Generalizando y llamando
*n* al día en el que estamos desde el día 0 de la epidemia:

<p class="formula">
<span class="math">Infectados(n) = Infectados(n − 1) · x = x<sup>n</sup></span>
</p>

Este es el desarrollo exponencial del que se habla: el día (*n*) aparece como
exponente, lo que significa que cada día que pase multiplica la base (*x*) por
sí misma una vez más.

Lo que es evidente, llegados a este punto, es que el incremento de infectados
de un día depende del número de infectados del día anterior. A más gente
infectada, más personas con capacidad de infectar a otras.

## Las medidas de prevención

Pero todo esto considera que *x* es constante y no tiene por qué serlo. La
capacidad de una persona infectada para contagiar a otra depende de dos tipos
de factores:

- Factores propios de la enfermedad: cómo se contagia, la eficacia de contagio
  que tiene, etc.<a href="#fn1" id="fnref1"><sup>1</sup></a>

- Factores externos: Vacunación, protecciones, número de contactos entre las
  personas, etc.

Las características de la enfermedad podemos suponer que son unas concretas y
que no varían, pero el segundo caso, los factores externos, pueden cambiar con
facilidad, por lo que la distribución que hemos propuesto previamente, donde
*x* era constante, es sólo cierta cuando los factores externos no existen y la
enfermedad campa a sus anchas, descontrolada.

También significa, por otro lado, que tenemos capacidad para alterar la
distribución de la enfermedad si afectamos a los factores externos, ya sea para
bien o para mal.

Para saber si unas medidas están o no teniendo efecto, es lógico fijarse en si
la fórmula propuesta previamente sigue cumpliéndose. Es decir, si la curva de
contagio sigue comportándose como una exponencial, lo que significaría que la
enfermedad no está siendo alterada por factores externos o, de otro modo, que
camba a sus anchas.


## Problema 1: ¿Somos exponenciales?

El gráfico que representa una curva exponencial es conocido por casi todos,
pero difícil de interpretar. Tiene que ir hacia arriba, ¿pero cuánto?

Cuando se visualizan datos que siguen una distribución lineal, es fácil
comprobar de forma visual que los datos están, o no están, en esa línea
imaginaria, pero no es tan fácil hacerlo con una exponencial, porque las
personas de a pie no disponemos de suficiente visión geométrica como para
hacerlo.

## Problema 2: Los números grandes

El crecimiento exponencial no es fácil de interpretar para quien no está
acostumbrado, principalmente porque no es fácil de predecir el incremento de un
salto al siguiente. Por ejemplo, para una *x* de 3, del primer día al segundo
el incremento es de 2 (el día uno con 3 casos, hay 2 casos más que el día cero,
que tiene 1), del segundo al tercero de 6 y del tercero al cuarto de
18. Dicho así, es evidente que el incremento también está siendo multiplicado
por *x* en cada etapa, pero esto no es tan sencillo de ver cuando *x* no es un
número entero.

Por otro lado, el incremento, que es exponencial también, es difícil de
gestionar en términos de "más que ayer". En el caso expuesto para una *x* de
valor 3, se podría dar la noticia de que hay más casos nuevos que ayer si el
incremento fuera de 14 en el cuarto día y sería cierto, pero el crecimiento no
estaría siendo exponencial (o no al menos con la misma base) al ser menor que
18, por lo que la enfermedad no se estaría expandiendo de forma descontrolada.
Con números pequeños es fácil de comprobar a simple vista, pero no todo el
mundo está acostumbrado a trabajar con números grandes como para poder
comprender esto.

Los gráficos, además, son difíciles de leer cuando se llega a números muy
grandes, y las referencias visuales no ayudan a determinar si el incremento es
*x* veces el incremento anterior o menor.

Las exponenciales crecen tan rápido que en poco son altísimas y tienen
incrementos descomunales. Si en un momento la transmisión de la enfermedad se
redujera, manteniendo la forma exponencial pero con una base menor, en una
gráfica parecería que la enfermedad ya no se extiende. Comparar números grandes
con pequeños es un error.

## Problema 3: El tiempo

Casi todas las gráficas que se muestran (y hasta nuestra propia formulita)
trabajan por unidad de tiempo. Es decir: muestran los casos por día. Pero si
lo que necesitamos conocer es si las medidas tomadas son efectivas, es más
razonable fijarse en lo que hemos denominado *x*, y, sobre todo, en la parte
ésta que podemos alterar, ya que no podemos cambiar el pasado y poco podemos
hacer con las características propias de la enfermedad.

Lo lógico, si lo que se pretende es hacer un análisis razonable de las medidas
que se están aplicando es prestar atención a lo que está en nuestra mano. Es
cierto que es un drama grave y que es duro ver morir cada día a más gente, pero
es algo que ahora mismo es tarde para cambiar. Publicar números de muertos e
infectados y recordarnos a diario que la cifra sigue subiendo no hace mucho más
que preocuparnos e indignarnos aún más.

Para poder analizar esa *x* en detalle hay que extraerla de la fórmula de algún
modo y representarla de modo que se aprecie claramente su evolución. El tiempo,
a veces, no es la mejor forma de hacerlo.

## Problema 4: Diferencias de criterio

Existen cantidad de comparaciones de datos entre países y la verdad es que no
sirven de mucho más que de mirar con envidia a los que mejor van y con
superioridad a los que peor.

La mayor parte de estos gráficos, al menos, se han tomado la molestia de
iniciarlos en el momento del primer contagio, para que se alineen en el mismo
punto de la epidemia. Dicho de otra forma, se visualizan respecto a nuestra *n*
y no respecto al día del calendario en el que estamos. Este pequeño cambio
ayuda a solventar el problema anterior en cierta medida, ya que la *n* puede
mostrar el punto de la enfermedad en el que se está, pero existen mejores
formas de hacerlo.

El mayor problema de las comparativas por país reside en el criterio que los
países han escogido para contar sus infectados y fallecidos. En algunos países
se realizan tests y en otros no se realizan más que en los casos graves. En
algunos países se consideran fallecidos por culpa de la enfermedad todas las
personas que fallecen por una patología asociada mientras que en otros sólo
los que han fallecido por consecuencia directa del virus.

Estas diferencias de criterio pueden hacer que las comparativas no sean
limpias y puedan confundirnos. Aunque también existe una solución parcial para
esto.

## Problema 5: El mundo real

El último de estos problemas puede resumirse con la siguiente frase:

> En teoría, la práctica y la teoría son lo mismo. En la práctica no.

Resulta que por mucho que hayamos supuesto que la curva es exponencial y que
sobre el «papel» todo tenga sentido, la realidad es que *x* no sólo deja de ser
constante por nuestra incidencia en ella, también sufre alteraciones debidas a
cambios en el entorno y porque nuestros números no son la realidad, sino
muestras de ella. La enfermedad no atiende a horarios y la gente no se contagia
de forma perfectamente uniforme a lo largo del día.  Algunos infectados que
«tocaba» que se infectasen hoy se infectan mañana, porque lo hacen de
madrugada, otro día las infecciones son más madrugadoras y caen en el día
previo, etc.

Estas alteraciones nos complican aún más la vista alterando nuestra bonita
exponencial por algo un poco distinto. Ahora es más complejo ver las
desviaciones de la exponencial porque es más difícil saber si las alteraciones
son debido al muestreo o si son debido a otros factores.

La exponencial con la que hemos trabajado no es más que una aproximación a cómo
debería comportarse la pandemia, no la realidad. No lo olvidemos.

## ¿Qué hacer?

Queremos entonces analizar nuestra *x*, o al menos poder mostrarla de un modo
que se vea correctamente. Podemos tratar de aislarla de modo que sea lo único
que se muestre, y no es especialmente difícil hacerlo. Mostrar, por ejemplo, el
incremento de infectados vs el número de infectados es suficiente forma para
ver esa *x* de alguna manera. Ya que, jugando con la matemática se puede
obtener algo así. Definiendo el incremento de infectados como la resta de los
infectados el día *n* con los del día previo: 

<p class="formula">
<span class="math">ΔInfectados(n) = Infectados(n) - Infectados(n − 1) = x<sup>n</sup> - x<sup>n-1</sup> </span>
</p>

<p class="formula">
<span class="math">ΔInfectados(n) = x<sup>n</sup> - x<sup>n</sup>/x = Infectados(n) - Infectados(n)/x = (1 - 1/x) · Infectados(n)</span>
</p>

O, por verlo más claro:

<p class="formula">
<span class="math">ΔInfectados(n) = ((x - 1) / x ) · Infectados(n)</span>
</p>

Si *x* es constante, como proponíamos antes, esto no es más que la fórmula de
una recta, *y = m ·x*, donde *m* es la pendiente de la recta. En nuestra
relación de infectados *m*, la pendiente, sería *(x-1)/x*.

Entonces, si todo dicho es cierto y las características de la enfermedad son
las mismas para todos los países (no ha habido mutaciones agresivas), en los
periodos previos a tomar medidas, como el confinamiento, el gráfico del
incremento de infectados vs infectados debería ser **una recta** con una
pendiente que muestre la capacidad de contagio de la enfermedad.

Como las suposiciones que hemos hecho para llegar a esta gráfica partían de que
los infectados crecieran de forma exponencial, sólo será recta si es así, por
lo que será fácil saber si estamos o no en una curva de contagio exponencial.
**Problema 1 resuelto**.

Como ahora los datos no están representados respecto al tiempo, sino respecto a
la cantidad total de infectados, no parten de una medida arbitraria sino del
estado actual de la enfermedad. A pesar de que el **problema 3 se resuelva** de
este modo, puede que para las personas menos habituadas a ver gráficas ésta
pierda algo de significado, ya que el tiempo es una dimensión que continúa
apareciendo, pero de forma implícita.

Esa gráfica idílica además sería capaz de romper el sesgo por de muestreo del
país porque el sesgo se aplicaría en **ambas** dimensiones. Si un país
estuviese muestreando a mucho menor porcentaje de población o sólo a los más
graves, ambas medidas serían pequeñas, la total y la incremental, pero
crecerían al mismo ritmo la una de la otra. Lo que **resuelve el problema 4**.

El problema de los números grandes puede ser, quizás el más difícil de
resolver, porque no es fácil cambiar la percepción de la humanidad en un par de
días. Sin embargo, la nueva gráfica rompe con la necesidad de leer una
exponencial, aunque compararse con los países inadecuados puede generar tales
diferencias en los números que sea imposible ver nada. Por ejemplo, EEUU, con
una población de 325 millones de habitantes tiene (en el momento que se
escriben estas líneas) muchísimos más infectados (medio millón) que Corea del
Sur (diez mil), que además de tener alrededor de un cuarto de los habitantes
(80 millones), ha tomado medidas muy efectivas (suponiendo que los datos sean
correctos). Este tipo de diferencias en los datos hacen que los gráficos sean
muy difíciles de ver. Para resolverlas suele usarse el truco de la [escala
logarítmica](https://es.wikipedia.org/wiki/Escala_logar%C3%ADtmica). Los
logaritmos, la operación opuesta a la exponenciación, crece de forma contraria
a ésta: cuanto más alto es más despacio crece.  Además, si se elige la base del
logaritmo con precisión se puede hacer una escala muy elegante, en la que la
distancia de 0 a 10 es la misma que de 10 a 100 o de 100 a 1000, etc.

Al visualizar la gráfica en escala logarítmica, se atraen los datos más
extremos hacia su centro, eliminando las deformaciones que los números grandes
generan y permitiéndonos así centrarnos en lo importante: la tendencia. Podemos
decir que esta elección **resuelve el problema 2** también, pero hay que ser
conscientes de que ahora las escalas son distintas y puede que nos hayamos
generado un pequeño nuevo problema de percepción al encoger los valores grandes
y agrandar los pequeños, pero en nuestro caso, al estar analizando una recta,
el resultado tras aplicar la nueva escala será una recta también y no
deberíamos tener mucho problema.

Con **El Mundo Real™** poco se puede hacer. Para mostrar correctamente esas
alteraciones se puede, por ejemplo, hacer una media de los últimos días y así
eliminar el ruido generado y tener una visión más clara de la tendencia. Lo que
ocurre con eso es que los datos que se muestran no son *reales* del todo. No
importa que no lo sean, lo importante es analizar si la tendencia se cumple, y
esta media ayuda a visualizarla. Quien se queje porque los datos no son los
reales puede buscar consuelo en que no iban a serlo aunque no se aplicase la
media. La vida es así de cruel a veces. La media **no resuelve el problema 5 en
su totalidad, pero se acerca** bastante.

Combinando todo esto podemos trazar esa gráfica y ver si realmente se entiende
tan bien como adelantábamos.
{% endfilter %}
{% endfilter %}

{% filter translate %}
<div class="graphs container p-2">

    <div class="columns">
        <div class="col-8 col-sm-12 data-graph">
            <em>Preparando visualización.<br>
                Esta visualización requiere de JavaScript y de un navegador
                moderno.</em>
            <div class="loading loading-lg"></div>
        </div>
        <div class="col-4 col-sm-12 p-2">
            <div class="columns">
                <div class ="col-sm-6 col-12 data-selector"></div>
                <div class ="column data-tooltip"></div>
            </div>
        </div>
    </div>
    <div class="columns">
        <div class="column help">
        </div>
        <div class="column text-right help-block">
            <div class="popover popover-left text-left">
                <button class="btn btn-link help-text">{{_("+Info")}}</button>
                <div class="popover-container help-card">
                    <div class="card">
                        <div class="card-body">{%filter translate%}
                            <p>El gráfico muestra la <em>media</em> del incremento
                                de casos de los últimos 7 días naturales.</p>
                            <p>Para marcar una línea del gráfico pasa el ratón
                                por encima o haz click en el país correspondiente.
                            </p>
                            <p>Dispones también de un resumen de los datos del
                                país seleccionado.</p>
                            <p>Los datos se cargan de
                                <a href="https://github.com/pomber/covid19">aquí</a>
                                y si no está disponible se carga una copia local del
                                día 11 de abril de 2020.</p>
                            <p>Puedes ver <a href="https://www.youtube.com/watch?v=54XLXg4fYsc">este vídeo</a>
                            de MinutePhysics para saber más.</p>
                            {%endfilter%}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
{%endfilter%}

{%filter translate%}
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p>Normalmente descritos por el <em>ritmo reproductivo básico</em> de la enfermedad, que mide la potencial capacidad de la enfermedad para expandirse.<a href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
{% endfilter %}

<p class="text-right">
Ekaitz Zarraga<br>
10 de Abril de 2020
</p>
{% endset %}


{% set post_includes %}
<style>
.axis-label{
  font-size: 70%;
}
.data-tooltip, .data-selector{
  margin: 1em;
}
p.formula {
  text-align: center;
}
p.formula > .math{
  padding: 0.7em;
  margin: 1.5em;
}
.math{
  font-style: italic;
}

.axis--y path {
  display: none;
}
.axis--y line {
  display: none;
}
.loading{
  text-align: center;
}
.help-text{
  cursor: help;
}
.help-block{
  font-size: smaller;
}
.help-card{
  max-width: 70vw;
}
</style>
{% endset %}

{% set post_scripts %}
<script src="/static/js/d3.v5.min.js"></script>
<script src="/static/projects/variedades/covid/covid.js"></script>
{% endset %}
