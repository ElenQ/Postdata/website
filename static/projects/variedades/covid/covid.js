// set the dimensions and margins of the graph
var margin = {top: 10, right: 0, bottom: 30, left: 60},
    width = 400 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

// append the svg object to the body of the page
let selector = ".data-graph";
let selected = d3.select(selector).selectAll("*").remove();
var svg = d3.select( selector )
  .append("svg")
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr("viewBox", "0 0 " + (width + margin.left + margin.right).toString() +
                        " " + (height + margin.top + margin.bottom).toString())
    .attr("max-height", "600px")
  .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

var countries = ["Spain",
                 "Korea, South",
                 "United Kingdom",
                 "France",
                 "Italy",
                 "Portugal",
                 "Germany",
                 "US"];
var countries_ES = ["España",
                    "Corea del Sur",
                    "Reino Unido",
                    "Francia",
                    "Italia",
                    "Portugal",
                    "Alemania",
                    "EEUU"];

function protectLog(val, isLog){
  return val>1 && isLog ?val:1;
}

function id(string){
  return string.replace(/[^a-z]/gi,"-");
}

function data_loaded(data_received){
  // Prepare data
  data = {};
  for(let c in countries){
    let x = data_received[countries[c]];
    x.sort(
      function(a, b){
        return a["confirmed"]-b["confirmed"];
      });
    x.forEach(
      function(d,i,arr){
        let confirmed = d.confirmed;
        let deaths    = d.deaths;
        let recovered = d.recovered;

        let deaths_inc    = 0;
        let recovered_inc = 0;
        let confirmed_inc = 0;
        let confirmed_inc_avg = 0;

        if(i>0){
          confirmed_inc = confirmed - arr[i-1].confirmed;
          deaths_inc    = deaths - arr[i-1].deaths;
          recovered_inc = recovered - arr[i-1].recovered;
        }
        let l = 7;
        if(l<i){
          confirmed_inc_avg = (confirmed - arr[i-l].confirmed)/l;
        }
        d["country"] = countries_ES[c];
        d["confirmed"] = protectLog(confirmed, true);
        d["confirmed_inc"] = protectLog(confirmed_inc, true);
        d["confirmed_inc_avg"] = protectLog(confirmed_inc_avg, true);
        d["deaths"] = protectLog(deaths, true);
        d["deaths_inc"] = protectLog(deaths_inc, true);
        d["recovered"] = protectLog(recovered, true);
        d["recovered_inc"] = protectLog(recovered_inc, true);
        d["date"] = d.date;
      });
    data[x[0]["country"]] = x;
  }

  // Get maximums for axis
  let maxByKey = function (key){
    return function(acc, x){
      m = d3.max(x, function(d){return d[key];});
      if(acc > m){
        return acc;
      }else{
        return m;
      }
    }
  };


  // TODO Maybe?
  // Change code below, use keys as parameter
  // MAYBE NOT: better just use the `l` variable on top as an argument
  let xmax = Object.values(data).reduce( maxByKey("confirmed"), 0);
  let ymax = Object.values(data).reduce( maxByKey("confirmed_inc_avg"), 0);

  // Make scales
  // TODO Maybe? make selectable in log or linear

  // Add X axis
  var x = d3.scaleLog().range([0, width])
    .domain([1, xmax]);
  svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x).ticks(5));
  // Add Y axis
  var y = d3.scaleLog().range([ height, 0 ])
    .domain([1, ymax]);
  svg.append("g")
    .call(d3.axisLeft(y));



  // Color
  var color = d3.scaleOrdinal(d3.schemeCategory10);

  // Draw the line
  var line = d3.line()
    .x(function(d) { return x(d["confirmed"]);})
    .y(function(d) { return y(d["confirmed_inc_avg"]);});

  // Line style
  let opacity             = 0.6;
  let stroke_width        = 1.5;
  let opacity_chosen      = 1;
  let stroke_width_chosen = 3.5;


  // Highlighting function for selections
  function highlight(country){
    let d = data[country];

    d3.selectAll(".highlight")
      .remove();
    d3.selectAll(".line")
      .style("opacity", opacity)
      .attr("stroke-width", stroke_width);

    svg.selectAll(".highlight")
      .data([d])
      .enter()
      .append("path")
      .attr("class", "highlight")
      .attr("stroke","white")
      .attr("fill","none")
      .attr("stroke-linecap", "round")
      .attr("stroke-linejoin", "round")
      .attr("stroke-width", stroke_width_chosen + 2)
      .attr("d", line)
      .style("opacity", 0.7);

    // Clean tooltip
    d3.select("div.data-tooltip").selectAll("*").remove();
    // Create tooltip
    var div = d3.select("div.data-tooltip")
      .selectAll("div")
      .data([d])
      .enter().append("div")
      .html(function(d){
        let last = d[d.length-1]
        return "<strong>"  +   d[0].country         + "</strong><br>" +
          "Fecha: " +   last.date   + "<br>" +
          "Casos confirmados: " +   last.confirmed + "<br>" +
          "Recuperaciones: " +   last.recovered + "<br>" +
          "Muertes: " +   d[d.length-1].deaths + "<br>";
      });

    d3.select("#"+ id(country))
      .raise()
      .style("opacity", opacity_chosen)
      .attr("stroke-width", stroke_width_chosen);
  }


  // Add lines
  svg.selectAll(".line")
    .data(Object.values(data))
    .enter()
    .append("path")
    .attr("class", "line")
    .attr("id", function(d){ return id(d[0].country); })
    .attr("d", line)
    .attr("fill", "none")
    .attr("stroke",function(d,i){return color(i);})
    .attr("stroke-width", stroke_width)
    .attr("stroke-linecap", "round")
    .attr("stroke-linejoin", "round")
    .style("opacity", opacity)
    .on("mouseover", function(d){
      highlight(d[0].country);
    })
    .on("mouseout", function(_){
    });

  // Axis labels
  svg.append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 0 - margin.left)
    .attr("x",0 - (height / 2))
    .attr("dy", "1em")
    .attr("fill", "currentColor")
    .style("text-anchor", "middle")
    .attr("class", "axis-label")
    .text("Incremento de casos confirmados (media semanal)");
  svg.append("text")
    .attr("x", width / 2 )
    .attr("y", height + margin.bottom )
    .attr("fill", "currentColor")
    .style("text-anchor", "middle")
    .attr("class", "axis-label")
    .style("text-anchor", "middle")
    .text("Casos confirmados (total)");

  highlight("España");
  // Initialize selector
  var select = d3.select("div.data-selector")
    .selectAll(".countries")
    .data(countries_ES)
    .enter()
    .append("button")
    .style("width", "100%")
    .style("background-color", "transparent")
    .style("color", function(d,i){return color(i);})
    .attr("value", function(d){return d;})
    .text(function(d){return d})
    .on("click", function(d){
      d3.selectAll(".data-selector button.active").classed("active", false);
      d3.select(this).classed("active", true);
      highlight(d);
    });
}

d3.json("https://pomber.github.io/covid19/timeseries.json")
  .then( data_loaded )
  .catch( function(){
    d3.json("/static/projects/variedades/covid/data.json").then(data_loaded);
  });
