// set the dimensions and margins of the graph
var margin = {top: 10, right: 50, bottom: 50, left: 60},
    width = 400 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

// append the svg object to the body of the page
let selector = ".data-graph";
d3.select(selector).selectAll("*").remove();
var svg = d3.select( selector )
  .append("svg")
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr("viewBox", "0 0 " + (width + margin.left + margin.right).toString() +
                        " " + (height + margin.top + margin.bottom).toString())
    .attr("max-height", "600px")
  .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");


function lighter(col){
  var color = d3.hsl(col);
  color.s -= 0.03;
  color.l += 0.18;
  return color;
}

function data_loaded(d){
  var stroke_width         = 1.2;
  var stroke_width_thin    = 1.7 / 2;
  var radius               = 3.1;
  var dosis_extra          = 6 / 5;
  var dosis_for_completion = 2;

  // convert date in data to Date
  d.map(function(x){x.date = new Date(x.date); return x});

  // Scale X
  var x = d3.scaleTime()
    .range([0,width])
    .domain([d[0].date, d[d.length-1].date]);
  // Axis X
  svg.append("g")
    .attr("class", "axis")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x)
      .tickFormat(d3.timeFormat("%Y-%m-%d")))
    .attr("font-size", "8.5")
    .selectAll("text") 
    .style("text-anchor", "end")
    .attr("dx", "-.8em")
    .attr("dy", ".15em")
    .attr("transform", "rotate(-65)");

  // Scale Y
  var y = d3.scaleLinear()
    .range([height, 0])
    .domain([0, (d[d.length-1].distribuidas * dosis_extra) * 1.10])
  // Axis Y
  svg.append("g")
    .attr("class", "axis")
    .call(d3.axisLeft(y))
    .attr("font-size", "8.5")
  ;

  // Colors
  var color = d3.scaleOrdinal(d3.schemeCategory10);

  function labels(d, accessor, name){
    // etiquetas
    svg.selectAll(".label-"+name)
      .data(d)
      .enter()
      .append("text")
      .attr("class", (d,i,e)=> "label-"+i)
      .classed("label label-"+name, true)
      .text(d=> new Number(accessor(d)).toLocaleString())
      .attr("x", d=>x(d.date))
      .attr("y", d=>y(accessor(d)))
      .attr("dy", -10)
      .attr("dx", 10)
      .attr("pointer-events", "none")
      .attr("fill", "black")
      .style("font-size", "6.5pt")
      .style("visibility", "hidden");

    svg.selectAll(".pointer-all")
      .data(d)
      .enter()
      .append("path")
      .attr("class", (d,i,e)=> "label-"+i)
      .classed("label", true)
      .attr("d", function(d){
        return "M "+ x(d.date) + "," + y(accessor(d)) + " l 8 -8" + " h 25";
      })
      .attr("fill", "none")
      .attr("stroke", "black")
      .attr("stroke-width", stroke_width_thin)
      .style("visibility", "hidden");

    svg.selectAll(".pointer-all")
      .data(d)
      .enter()
      .append("circle")
      .attr("class", (d,i,e)=> "label-"+i)
      .classed("label", true)
      .attr("r", stroke_width_thin)
      .attr("cx", d=>x(d.date))
      .attr("cy", d=>y(accessor(d)))
      .attr("fill", "black")
      .attr("stroke", "black")
      .attr("stroke-width", stroke_width_thin)
      .style("visibility", "hidden");
  }

  function area_element(d, area, name, color){
    // area
    svg.selectAll(".area-" + name)
      .data([d])
      .enter()
      .append("path")
      .classed("area area-" + name, true)
      .attr("d", area)
      .attr("fill", lighter(color))
      .attr("stroke-width", stroke_width)
  }

  // Generic function to repeat similar object creation.
  // USES ENVIRONMENT
  function data_block(d,accessor,name, color){
    // area
    let area = d3.area()
      .x(d => x(d.date))
      .y0(d=> y(0))
      .y1(d=> y(accessor(d)));
    area_element(d, area, name, color);

    // línea
    svg.selectAll(".line-" + name)
      .data([d])
      .enter()
      .append("path")
      .classed("line line-"+ name, true)
      .attr("d", d3.line().x(d=>x(d.date)).y(d=>y(accessor(d))))
      .attr("fill", "none")
      .attr("stroke-width", stroke_width)
      .attr("stroke", "white")
    // puntos
    svg.selectAll(".puntos-"+name)
      .data(d)
      .enter()
      .append("circle")
      .attr("class", (d,i,e)=> "puntos-"+i)
      .classed("puntos puntos-"+name, true)
      .attr("cx", function(d) { return x(d.date)})
      .attr("cy", function(d) { return y(accessor(d))})
      .attr("r", radius)
      .attr("fill", color)
      .attr("stroke", "white")
      .attr("stroke-width", stroke_width)
      .on("mouseover", function(d, i, e){
        // Visualiza label actual
        d3.selectAll(".label").style("visibility", "hidden");
        d3.selectAll(".label-"+i).style("visibility", "visible");
      })
    labels(d, accessor, name);
  }

  // AREA de vacunas totales si se extrae la 6a dosis del vial
  let areaDistExtra = d3.area()
    .x(d => x(d.date))
    .y0(d=> y(d.distribuidas))
    .y1(d=> y(d.distribuidas * dosis_extra));
  area_element(d, areaDistExtra, "dist-extra", lighter(color(0)))

  // DISTRIBUIDAS
  data_block(d, x=>x["distribuidas"], "dist", color(0));
  // APLICADAS
  data_block(d, x=>x["vacunas"], "vac", color(1));

  // AREA de vacunas totales necesarias para el ciclo completo
  let areaCompExtra = d3.area()
    .x(d => x(d.date))
    .y0(d=> y(d.completas))
    .y1(d=> y(d.completas * dosis_for_completion));
  area_element(d, areaCompExtra, "comp-extra", lighter(color(2)))

  // Etiquetas
  labels(d, (x)=> x.completas * dosis_for_completion, "comp-extra")
  labels(d, (x)=> x.distribuidas * dosis_extra,"dist-extra")

  // CICLO COMPLETO
  data_block(d, x=>x["completas"], "comp", color(2));


  // Marcar el último valor
  d3.selectAll(".label-" + (d.length - 1)).style("visibility", "visible");

  // Marcar la fecha del artículo
  let x_line = x(new Date("2021-03-23T00:00:00.000000"))
  let y_line = y(10500000)
  svg.selectAll(".line-date-article")
    .data([false])
    .enter()
    .append("path")
    .classed("line-date-article", true)
    .attr("d", "M " + x_line + ","+ y(0) +" V " + y_line + "h -30" )
    .attr("fill", "none")
    .attr("stroke-width", stroke_width_thin)
    .attr("stroke", "brown")
    .attr("pointer-events", "none")
  svg.selectAll(".label-date-article")
    .data([false])
    .enter()
    .append("text")
    .classed("label-date-article", true)
    .text("Fecha pub.")
    .attr("x", x_line)
    .attr("y", y_line)
    .attr("dy", -2)
    .attr("pointer-events", "none")
    .attr("fill", "brown")
    .style("text-anchor", "end")
    .style("font-size", "6.5pt");


  // colocar las etiquetas arriba
  svg.selectAll(".label").raise();
}

d3.json("/static/projects/variedades/covid-2/DATA.json")
  .then(data_loaded)
  .catch(function(){
    console.error("No se pueden cargar los datos por alguna razón :(");
  })
