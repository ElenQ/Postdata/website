//Width and height of the graph
var graphw = 400;
var graphh = 170;
//Width and height of the map
var mapw = 600;
var maph = 550;

// Colors
var fillcolor = "steelblue";
var selectedcolor = "brown";
var mediancolor = "darkblue";

// Register some of the elements for future reference
var registry = {};
registry["map"]     = [];
registry["graph"]   = [];
registry["tooltip"] = [];

// Filenames with the data
var files = [
  { name: "Precio mensual (€/mes)",
    value: "/static/projects/sociedad_bilbaina/02-ikerYgorka/price_euro_distrito.csv" },
  { name: "Precio mensual por metro cuadrado (€/mes/m²)",
    value: "/static/projects/sociedad_bilbaina/02-ikerYgorka/price_euro_per_m2_distrito.csv" },
  { name: "Tamaño de la vivienda (m²)",
    value: "/static/projects/sociedad_bilbaina/02-ikerYgorka/size_m2_distrito.csv"}
];

function selectDistrict(district_code){
  // Tooltips
  for( i in registry.tooltip){
    d3.select(registry.tooltip[i]).style("display", "none");
  }
  d3.select(registry.tooltip[district_code]).style("display", "inline-block");

  // Map
  for( i in registry.map){
    d3.select(registry.map[i]).style("fill", fillcolor);
  }
  d3.select(registry.map[district_code]).style("fill", selectedcolor);

  // Graph
  for( i in registry.graph){
    d3.select(registry.graph[i]).style("fill", fillcolor);
  }
  d3.select(registry.graph[district_code]).style("fill", selectedcolor);

  // Mark selected
  registry.selected = district_code;
}

function onchange() {
  selectValue = d3.select('select').property('value')
  visualize(".data-graph", selectValue, graphw, graphh).then(function(){
    if(registry.selected != undefined)
      selectDistrict(registry.selected);
  });
};

var select = d3.select("select.data-selector")
  .attr("title", "Selecciona estadística")
  .on("change", onchange);
select
  .selectAll(".options")
  .data(files).enter()
  .append("option")
  .attr("value", function(d){return d.value})
  .text(function(d){return d.name});


function visualize(selector, filename, w, h){
  // Download the data and stuff
  return d3.csv(filename,
    function(d){
      for(var i in d){
        if( i == "district"){
          continue;
        }
        d[i] = parseFloat(d[i]);
      }
      return d;}
  ).then(
    function(data) {
      data.sort(
        function(a,b){
          return b.mean - a.mean;
        });

      //Empty svg
      d3.select(selector).selectAll("*").remove();

      //Prepare the SVG
      var svg = d3.select(selector)
        .append("svg")
        .attr("preserveAspectRatio", "xMinYMin meet")
        .attr("viewBox", "0 0 "+ w.toString() + " " + h.toString())
        .style("max-height", "500px"),
        margin = {top: 10, right: 10, bottom: 30, left: 130},
        width = w - margin.left - margin.right,
        height = h - margin.top - margin.bottom;

      var g = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // Scales
      var y = d3.scaleBand().rangeRound([0, height]).padding(0.1),
          x = d3.scaleLinear().rangeRound([0, width]);

      //Empty tooltip
      d3.select("div.data-tooltip").selectAll("*").remove();
      // Define the div for the tooltip
      var div = d3.select("div.data-tooltip")
        .selectAll("div")
        .data(data)
        .enter().append("div")
        .html(function(d){
          return "<strong>"+        d.district          + "</strong><br>" +
                 "Media: "+         d.mean.toFixed(2)   + "<br>" +
                 "Mediana: " +      d.median.toFixed(2) + "<br>" +
                 "Desv. Típica: " + d.std.toFixed(2)    ;
        })
        .style("display", "none")
        .each(function(d, i, els){registry.tooltip[d.district_code] = els[i]});

      // Prepare the graph
      x.domain([0, d3.max(data, function(d) { return d.mean + d.std;})]);
      y.domain(data.map(function(d) { return d.district; }));

      g.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x).ticks(5));

      g.append("g")
        .attr("class", "axis axis--y")
        .call(d3.axisLeft(y));

      // Mean
      g.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        .attr("y", function(d) { return y(d.district); })
        .attr("x", function(d) { return 0; })
        .attr("height", y.bandwidth())
        .attr("width", function(d) { return x(d.mean); })
        .attr("fill", fillcolor)
        .each(function(d, i, els){registry.graph[d.district_code] = els[i]});

      // Standard Deviation
      var std = g.selectAll(".std")
        .data(data)
        .enter();

      std.append("line")
        .attr("y1", function(d){ return y(d.district) + y.bandwidth()/2;})
        .attr("x1", function(d){ return x(d.mean - d.std);})
        .attr("y2", function(d){ return y(d.district) + y.bandwidth()/2;})
        .attr("x2", function(d){ return x(d.mean + d.std);})
        .style("stroke-width", "1px")
        .style("stroke", "black");

      std.append("line")
        .attr("y1", function(d){ return y(d.district) + y.bandwidth()/4;})
        .attr("x1", function(d){ return x(d.mean - d.std);})
        .attr("y2", function(d){ return y(d.district) + y.bandwidth()*3/4;})
        .attr("x2", function(d){ return x(d.mean - d.std);})
        .style("stroke-width", "1px")
        .style("stroke", "black");

      std.append("line")
        .attr("y1", function(d){ return y(d.district) + y.bandwidth()/4;})
        .attr("x1", function(d){ return x(d.mean + d.std);})
        .attr("y2", function(d){ return y(d.district) + y.bandwidth()*3/4;})
        .attr("x2", function(d){ return x(d.mean + d.std);})
        .style("stroke-width", "1px")
        .style("stroke", "black");

      // Median
      g.selectAll(".median")
        .data(data)
        .enter()
        .append("line")
        .attr("y1", function(d){ return y(d.district);})
        .attr("x1", function(d){ return x(d.median);})
        .attr("y2", function(d){ return y(d.district) + y.bandwidth();})
        .attr("x2", function(d){ return x(d.median);})
        .style("stroke-width", "1px")
        .style("stroke", mediancolor);

      // Event catcher
      g.selectAll(".catcher")
        .data(data)
        .enter()
        .append("rect")
        .attr("y", function(d) { return y(d.district); })
        .attr("x", function(d) { return -130; })
        .attr("height", y.bandwidth() + y.bandwidth()*0.1)
        .attr("width", function(d) { return w; })
        .attr("fill", "transparent")
        .style("cursor","pointer")
        .on("click touch", function(d){
          selectDistrict(d.district_code);
        });

    }).catch(console.error);
}

function createMap(w, h){
  //Load in GeoJSON data
  return d3.json("/static/projects/sociedad_bilbaina/02-ikerYgorka/distritos_latlon.json").then(
    function(json) {
      //Define map projection
      var projection = d3.geoMercator()
        .translate([0, 0])
        .scale(1);

      //Define path generator
      var path = d3.geoPath()
        .projection(projection);

      //Create SVG element
      var svg = d3.select("div.data-map")
        .append("svg")
        .attr("preserveAspectRatio", "xMinYMin meet")
        .attr("viewBox", "0 0 "+ w.toString() + " " + h.toString())
        .style("max-height", "700px");

      // Calculate bounding box transforms for entire collection
      var b = path.bounds( json ),
        s = .95 / Math.max((b[1][0] - b[0][0]) / w, (b[1][1] - b[0][1]) / h),
        t = [(w - s * (b[1][0] + b[0][0])) / 2, (h - s * (b[1][1] + b[0][1])) / 2];

      // Update the projection
      projection
        .scale(s)
        .translate(t);

      //Bind data and create one path per GeoJSON feature
      svg.selectAll("path")
        .data(json.features)
        .enter()
        .append("path")
        .attr("d", path)
        .style("fill", fillcolor)
        .style("stroke","white")
        .style("cursor","pointer")
        .each(function(d, i, els){registry.map[d.properties.code] = els[i]})
        .on("click touch", function(d,i){
          selectDistrict(d.properties.code);
        });

      // Mediaqueries to remove the labels in smaller devices?
      svg.selectAll("text")
        .data(json.features)
        .enter().append("text")
        .attr("class","label")
        .attr("transform", function(d) { return "translate(" + path.centroid(d) + ")"; })
        .text(function(d) { return d.properties.district; })
        .style("text-anchor", "middle")
        .style("font-size", ".8rem")
        .style("pointer-events", "none");
    }).catch(console.error);
}

var graph = visualize(".data-graph", "/static/projects/sociedad_bilbaina/02-ikerYgorka/price_euro_distrito.csv", graphw, graphh);
var map = createMap(mapw, maph);

Promise.all([graph, map]).then(function(){selectDistrict("6");});
