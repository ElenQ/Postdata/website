# Postdata.biz website

This site can be visited at https://postdata.biz

This project contains everything necessary to create postdata.biz website using
[`schiumato` Static Site Generator](https://gitlab.com/ElenQ/schiumato).


## Usage

Install the dependencies:

```
npm i
```

Now install [schiumato](https://gitlab.com/ElenQ/schiumato) (may require
superuser access):

```
npm i -g schiumato
```

You can create the website by running this command in the root folder of the
repository:

```
schiumato create
```

[Schiumato's documentation](https://gitlab.com/ElenQ/schiumato) explains the
directory structure and everything else you need to understand how it works.


## Advanced usage

This project uses
[Spectre.css](https://picturepan2.github.io/spectre/getting-started.html) for
the style. If you want to build a custom version of the CSS (which is exactly
what we did), you can make use of the `style_src` folder. In that folder you
can write some `scss` files (follow Spectre.css's documentation for that) and
they you can build them using `sass`. This workflow is already automated in the
`buildall.sh` script and you can use it as a reference.

> There's no need to run the `sass` compiler every time and we already packaged
> the compiled CSS files in the static folder, the CSS compilation is only
> needed if you want to override the default style.


# License

This project has two licenses:

- The contents of the website licensed under the terms of Creative Commons
  Attribution-Share Alike 4.0 International.
- The source code is MIT licensed.

This distinction is important.
